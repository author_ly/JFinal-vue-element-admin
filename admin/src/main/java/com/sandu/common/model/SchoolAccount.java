package com.sandu.common.model;

import com.sandu.common.model.base.BaseSchoolAccount;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class SchoolAccount extends BaseSchoolAccount<SchoolAccount> {
	public static final SchoolAccount dao = new SchoolAccount().dao();
}
